<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Inventory;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class InventoryController extends Controller
{
    protected $inventory;

    public function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $inventories = $this->inventory->all();

        return response()->json([
            'error' => false,
            'inventories' => $inventories,
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $inventory = $this->inventory->create($request->all());

        return response()->json([
            'error' => false,
            'inventory' => $inventory,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $inventory = $this->inventory->find($id);

        return response()->json([
            'error' => false,
            'inventory' => $inventory,
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $inventory = $this->inventory->find($id);

        $inventory->item = $request->input('item');
        $inventory->description = $request->input('description');
        $inventory->quantity = $request->input('quantity');
        $inventory->price = $request->input('price');

        $inventory->save();

        return response()->json([
            'error' => false,
            'inventory' => $inventory,
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $inventory = $this->inventory->find($id);
        $inventory->delete();

        return response()->json([
            'error' => false,
            'message' => "Inventory $inventory->id успешно удален",
        ], Response::HTTP_OK);
    }
}
