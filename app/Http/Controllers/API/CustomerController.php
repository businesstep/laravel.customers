<?php

namespace App\Http\Controllers\API;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Http\Traits\CustomerTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CustomerController extends Controller
{
    use CustomerTrait;

    protected $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $customers = $this->customer->paginate(10);

        if (is_null($customers)) {
            return $this->notFound();
        }

        return response()->json([
            'error' => false,
            'customers' => $customers,
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request)
    {
        $customer = $this->customer->create($request->all());

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $customer = $this->customer->with('orders')
            ->with('orders.details')
            ->find($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, $id)
    {
        $customer = $this->customer->find($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        $customer->first_name = $request->input('fist_name');
        $customer->last_name = $request->input('last_name');
        $customer->email = $request->input('email');
        $customer->address = $request->input('address');

        $customer->save();

        return response()->json([
            'error' => false,
            'customer' => $customer,
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $customer = $this->customer->find($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        $customer->delete();

        return response()->json([
            'error' => false,
            'message' => "Customer $customer->id успешно удален.",
        ], Response::HTTP_OK);
    }

    /**
     * Create order
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function order(Request $request, $id)
    {
        $customer = $this->customer->find($id);

        if (is_null($customer)) {
            return $this->notFound();
        }

        $order = $customer->orders()->create([
            'order_date' => Carbon::now(),
            'order_notes' => $request->input('order_notes'),
        ]);

        $items = $request->input('items');

        foreach ($items as $item) {
            $order->details()->create([
                'inventory_id' => $item['inventory_id'],
                'quantity' => $item['quantity'],
            ]);
        }

        return response()->json([
            'error' => false,
            'order' => $order,
        ], Response::HTTP_CREATED);
    }
}
