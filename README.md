**Laravel 6 Rest API**

Rest API project that contains customers and their orders.

*using Laravel 6.2*

---

## Install

1. Clone **laravel.customers.git**: `git clone https://bitbucket.org/businesstep/laravel.customers.git`
2. Install Composer dependencies: `composer.phar install`
3. In .env file specify database access
4. Run migration: `php artisan migrate`
5. Run seed: `php artisan db:seed --class=DatabaseSeeder`
6. Install Node.js dependencies: `npm install`
7. Compile css, js scripts: `npm run dev` 

---

## Установка

1. Склонировать репозиторий **laravel.customers.git**: `git clone https://bitbucket.org/businesstep/laravel.customers.git`
2. Установить Composer зависимости, выполнив команду: `composer install`
3. В файле .env указать доступы к БД
4. Запустить БД миграции, выполнив команду: `php artisan migrate`
5. Запустить заполнение данными БД: `php artisan db:seed --class=DatabaseSeeder`
6. Установить Node.js зависимости, выполнив команду: `npm install`
7. Скомпилировать css и js скрипты, выполнив команду: `npm run dev`
